const path = require('path');
var webpack = require('webpack');
var BUILD_DIR = path.resolve(__dirname, 'dist/');
var APP_DIR = path.resolve(__dirname, 'src/');
// var Loaders = require('../webpack.loaders.js');

module.exports = {
    entry: APP_DIR + '/index.js',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    module:{
        rules:[
            {
                test:'/\.js$/' ,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ["es2015", "react", "env"]
                }
            }
        ]
    }
};
